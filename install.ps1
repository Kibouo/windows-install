# only run setup as admin
if (-not ([Security.Principal.WindowsPrincipal] `
            [Security.Principal.WindowsIdentity]::GetCurrent() `
    ).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Write-Output 'Run script as admin!'
    exit
}

# allow external script execution
Set-ExecutionPolicy AllSigned

# install chocolatey and our packages
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

function install_from_packages {
    param (
        [string]$type
    )

    $pkgs = Get-Content -Path ./packages/$type
    ForEach ($pkg in $pkgs) {
        choco install --confirm --accept-license $pkg
    }
}

install_from_packages 'base'

$daily_choice = read-host -prompt 'Install "daily" items? [y/N]'
if ($daily_choice -eq 'Y' -or $daily_choice -eq 'y') {
    install_from_packages 'daily'
}

$pentest_choice = read-host -prompt 'Install "pentest" items? [y/N]'
if ($pentest_choice -eq 'Y' -or $pentest_choice -eq 'y') {
    install_from_packages 'pentest'
}

# download and setup dotfiles
git clone https://gitlab.com/Kibouo/dotfiles.git $HOME/.dotfiles
Invoke-Expression $HOME/.dotfiles/deploy.ps1

# create .lnk's
function new_start_menu_lnk {
    param (
        [string]$command,
        [string]$params,
        [string]$lnk_filename,
        [string]$ico_path,
        [bool]$run_as_admin
    )

    $lnk_path = "${ENV:APPDATA}/Microsoft/Windows/Start Menu/Programs/${lnk_filename}"

    $wshell = New-Object -comObject WScript.Shell
    $lnk = $wshell.CreateShortcut($lnk_path)
    $lnk.TargetPath = $command
    $lnk.Arguments = $params
    $lnk.IconLocation = $ico_path
    $lnk.Save()

    if ($run_as_admin) {
        $bytes = [System.IO.File]::ReadAllBytes($lnk_path)
        $bytes[0x15] = $bytes[0x15] -bor 0x20 #set byte 21 (0x15) bit 6 (0x20) ON
        [System.IO.File]::WriteAllBytes($lnk_path, $bytes)
    }
}

# start menu
$wt_exe = (Get-Command wt.exe).source
new_start_menu_lnk "${ENV:APPDATA}/../Local/Microsoft/WindowsApps/wt.exe" 'new-tab --profile "Windows PowerShell" ; new-tab --profile "Arch"' 'Terminal.lnk' $wt_exe 0
new_start_menu_lnk "${ENV:APPDATA}/../Local/Microsoft/WindowsApps/wt.exe" 'new-tab --profile "Windows PowerShell"' '/Terminal - Admin.lnk' "${wt_exe}/../Images/terminal_contrast-black.ico" 1
# backslashes in argument for explorer, otherwise it doesn't know what to do
new_start_menu_lnk "${ENV:SYSTEMROOT}/explorer.exe" "${ENV:PROGRAMDATA}\chocolatey\lib\sysinternals\tools" 'SysInternals.lnk' "${ENV:SYSTEMROOT}/system32/imageres.dll, 7" 0

# auto start
new_start_menu_lnk "${HOME}/.config/autohotkey/main.ahk" '' 'Startup/autohotkey.lnk' "${ENV:SYSTEMROOT}/system32/imageres.dll, 2" 0

# powershell intellisense
Install-Module PSReadLine -RequiredVersion 2.1.0
